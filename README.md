README.md
===
Visitor_Pattern

특정 오브젝트에서 여러 오브젝트를 관리하면서 관리 오브젝트의 특정! 알고리즘에 접근하고 싶을때 사용

아래를 예로 설명한다.

![이미지](image/Visitor_Pattern_image.jpg)<br>

특정 오브젝트(Car) 에서 여러 오브젝트(Body, Engine, Wheel)을 관리하면서 관리 오브젝트의 특정 알고리즘에 접근하고 싶을때(CarStartVisitor, CarPrintVisitor) 사용

---

```c

// main만 보면된다.

int main()
{
	// 관리 Object
	Wheel mWheel_front_left("front_left");
	Wheel mWheel_front_right("front_right");
	Wheel mWheel_back_left("back_left");
	Wheel mWheel_back_right("back_right");
	Engine mEngine;

    // 주인 Object
	Car mCar;

	// Car가 관리 할 수 있도록 추가
	mCar.append(&mWheel_front_left);
	mCar.append(&mWheel_front_right);
	mCar.append(&mWheel_back_left);
	mCar.append(&mWheel_back_right);
	mCar.append(&mEngine);

	// 관리 Object의 특정 알고리즘만 쓰기위해서 Visitor추가
	CarPrintVisitor mCarPrintVisitor;
	CarStartVisitor mCarStartVisitor;

	// visitor accept - 알고리즘을 Go!!
	mCar.accept(&mCarPrintVisitor);
	mCar.accept(&mCarStartVisitor);
}

```