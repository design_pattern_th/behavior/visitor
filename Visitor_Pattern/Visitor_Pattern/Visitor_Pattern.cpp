﻿// Visitor_Pattern.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <list>

using namespace std;

class Visitor;

class Element
{
public:
	virtual void accept(Visitor* v) = 0;
};

class Wheel : public Element
{
public:
	Wheel(string name) : mName(name) {}

public:
	/*virtual*/ void accept(Visitor* v);
	const string getName() const { return mName; }

private:
	string mName;
};

class Engine : public Element
{
public:
	/*virtual*/ void accept(Visitor* v);
};

class Car : public Element
{
public:
	void append(Element* e) { mList.push_back(e); }
	/*virtual*/ void accept(Visitor* v);

private:
	list<Element*> mList;
};


class Visitor
{
public:
	virtual void visit(Wheel* e) = 0;
	virtual void visit(Engine* e) = 0;
	virtual void visit(Car* e) = 0;
};


class CarPrintVisitor : public Visitor
{
public:
	void visit(Wheel* e) override { cout << "Visiting " << e->getName() << " Wheel\n" << endl; }
	void visit(Engine* e) override { cout << "Visiting engine\n" << endl; }
	void visit(Car* e) override { cout << "Visiting car\n" << endl; }
};

class CarStartVisitor : public Visitor
{
public:
	void visit(Wheel* e) override { cout << "Kicking my " << e->getName() << " Wheel\n" << endl; }
	void visit(Engine* e) override { cout << "Starting my engine\n" << endl; }
	void visit(Car* e) override { cout << "Starting my car\n" << endl; }
};


void Wheel::accept(Visitor* v) { v->visit(this); }
void Engine::accept(Visitor* v) { v->visit(this); }
void Car::accept(Visitor* v) { 
	for (auto i = mList.begin(); i != mList.end(); ++i) {
		(*i)->accept(v);
	}

	v->visit(this); 
}


int main()
{
	// 부품 생성
	Wheel mWheel_front_left("front_left");
	Wheel mWheel_front_right("front_right");
	Wheel mWheel_back_left("back_left");
	Wheel mWheel_back_right("back_right");
	Engine mEngine;
	Car mCar;

	// 파트 추가
	mCar.append(&mWheel_front_left);
	mCar.append(&mWheel_front_right);
	mCar.append(&mWheel_back_left);
	mCar.append(&mWheel_back_right);
	mCar.append(&mEngine);

	// visitor 생성
	CarPrintVisitor mCarPrintVisitor;
	CarStartVisitor mCarStartVisitor;

	// visitor accept 처리
	mCar.accept(&mCarPrintVisitor);
	mCar.accept(&mCarStartVisitor);
}

